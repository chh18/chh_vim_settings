
# manage vim plugins
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
mkdir -p ~/.vim/autoload ~/.vim/bundle
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
# sourcing 
cp $HOME/chh_vim_settings/vimrc .vimrc
# install vim plugins
vim -c 'PluginInstall' -c 'qa!'
# verilog auto-complete snippets
mkdir $HOME/.vim/UltiSnips
cp $HOME/chh_vim_settings/verilog_snippets/* $HOME/.vim/UltiSnips/
vim -c 'UltiSnipsAddFiletypes verilog' -c 'UltiSnipsAddFiletypes systemverilog' -c 'UltiSnipsAddFiletypes verilogams' -c  'UltiSnipsEdit!' -c 'qa!'
vim -c '$HOME/.vim/bundle/vlog_inst_gen/plugin/vlog_inst_gen.vim' -c 'w ++ff=unix'
# spice syntax settings
git clone https://github.com/1995parham/vim-spice.git
#mv $HOME/vim-spice/syntax $HOME/.vim/
cp -r $HOME/chh_vim_settings/spice_settings/* $HOME/.vim/ 
# system verilog settings
 git clone https://github.com/vhda/verilog_systemverilog.vim
mv $HOME/verilog_systemverilog.vim/ $HOME/.vim/bundle/.
